


//

setTimeout(function () {

}, 0)

setTimeout(function () {

}, 1)

setTimeout(function () {

}, 2)

setTimeout(function () {

}, 3)

//

setTimeout(function () {

}, 101)

setTimeout(function () {

}, 10101)

setTimeout(function () {

}, 1010101)

//

setTimeout(function () {

}, 0.101)

setTimeout(function () {

}, 0.10101)

setTimeout(function () {

}, 0.1010101)

// which means that 1 time is good for all the programs, which is "now"


setTimeout(function () {
    // program 1, add 2 numbers, return nothing because the sum is implicit
    // in the runtime of this program
}, in_the_now(0.program_name, 0.number_1, 0.number_2))

setTimeout(function () {

}, in_the_now(0.program_name, 0.number_1, 0.number_2))

setTimeout(function () {

}, in_the_now(0.program_name, 0.number_1, 0.number_2))





